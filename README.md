# RAE359 #
## Distributed system ##

### Links:

##University Stuff: ##

*[Safari Books Online](http://safaribooksonline.com)

*[Distributed Systems In One Lesson](https://www.safaribooksonline.com/library/view/distributed-systems-in/9781491924914/)

*[Riga Technical University](https://www.rtu.lv/en)

*[Riga Technical University - For Students](https://ortus.rtu.lv/)

*[Riga Technical University - Sport Activities](https://www.rtu.lv/lv/sports)

*[Distance Education Center](http://tsc.edx.lv/)

## Time Killers: ##

*[You In Youtube](http://youtube.com)

*[Games Online](https://www.kongregate.com/)

*[Memes](https://www.memedroid.com/)

*[Free Movies Online](https://123movies.io/)

## Helping Tools: ##

*[Whatsapp Network Edition](https://web.whatsapp.com/)

*[Wikipedia](https://www.wikipedia.org/)

*[Wolfram|Alpha](https://www.wolframalpha.com/)

## Fake News: ##

*[CNN](http://edition.cnn.com/)

*[Washington Post](https://www.washingtonpost.com/)

*[ABC News](http://abcnews.go.com/)

## Homeworks Done: ##

*[Homework - Review of Distributed Systems](https://youtu.be/85IE9GBkBKo)